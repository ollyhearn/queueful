from fastapi import status, HTTPException, Depends, Header
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from jose import JWTError, jwt
from typing import Annotated, Union
from datetime import datetime, timezone, timedelta
from passlib.context import CryptContext
import uuid
import random
from io import BytesIO
from captcha.image import ImageCaptcha

from ...db import models
from . import schemas
from ...dependencies import get_db
from ...config import jwt_config

CAPTCHA_SYMBOLS = "abcdefghijklmnopqrstuvwxyz0123456789"

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password) -> str:
    return pwd_context.hash(password)


def get_user_by_id(db: Session, user_id: uuid.uuid4) -> models.User:
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_username(db: Session, username: int) -> models.User:
    return db.query(models.User).filter(models.User.username == username).first()


def authenticate_user(db: Session, username: str, password: str):
    user = get_user_by_username(db, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: Union[timedelta, None] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.now(timezone.utc) + expires_delta
    else:
        expire = datetime.now(timezone.utc) + timedelta(weeks=2)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode, jwt_config.SECRET_KEY, algorithm=jwt_config.ALGORITHM
    )
    return encoded_jwt


def create_user(db: Session, user_data: schemas.UserRegister) -> schemas.UserInDB:
    user = models.User(
        username=user_data.username,
        name=user_data.name,
        hashed_password=get_password_hash(user_data.password),
    )
    db.add(user)
    db.commit()
    return schemas.UserInDB.model_validate(user)


def get_current_user(
    token: Annotated[str, Depends(oauth2_scheme)],
    db: Annotated[Session, Depends(get_db)],
) -> schemas.UserInDB:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(
            token, jwt_config.SECRET_KEY, algorithms=[jwt_config.ALGORITHM]
        )
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = schemas.TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user_by_username(db, username=token_data.username)
    if user is None:
        raise credentials_exception
    return user


def get_current_user_or_none(
    db: Annotated[Session, Depends(get_db)],
    authorization: Annotated[Union[str, None], Header()] = None,
) -> Union[schemas.UserInDB, None]:
    try:
        if authorization:
            token = authorization.split()[1]
            payload = jwt.decode(
                token, jwt_config.SECRET_KEY, algorithms=[jwt_config.ALGORITHM]
            )
            username: str = payload.get("sub")
            if username is None:
                raise credentials_exception
            token_data = schemas.TokenData(username=username)
        else:
            return None
    except JWTError:
        return None
    user = get_user_by_username(db, username=token_data.username)
    return user


def get_current_active_user(
    current_user: Annotated[schemas.User, Depends(get_current_user)],
):
    if not current_user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


def create_anon_user(db: Annotated[Session, Depends(get_db)]) -> schemas.AnonUser:
    u = models.AnonymousUser()
    db.add(u)
    db.commit()
    # return schemas.AnonUser.model_validate(u)
    return u


def get_anon_user(
    db: Annotated[Session, Depends(get_db)],
    x_client_id: Annotated[Union[str, None], Header()] = None,
) -> schemas.AnonUser:
    if x_client_id:
        anon = (
            db.query(models.AnonymousUser)
            .filter(models.AnonymousUser.id == x_client_id)
            .first()
        )
        if anon:
            return anon
        raise HTTPException(
            status_code=status.HTTP_418_IM_A_TEAPOT,
        )
    return create_anon_user(db)


def patch_anon_name(
    data: schemas.AnonUserPatch,
    db: Annotated[Session, Depends(get_db)],
    x_client_id: Annotated[Union[str, None], Header()] = None,
) -> schemas.AnonUser:
    if x_client_id:
        anon = (
            db.query(models.AnonymousUser)
            .filter(models.AnonymousUser.id == x_client_id)
            .first()
        )
        if anon:
            setattr(anon, "name", data.name)
            db.commit()
            return anon
        raise HTTPException(
            status_code=status.HTTP_418_IM_A_TEAPOT,
        )
    return create_anon_user(db)


def get_captcha(
    captcha_id: uuid.UUID, db: Annotated[Session, Depends(get_db)]
) -> BytesIO:
    prompt = "".join(random.choice(CAPTCHA_SYMBOLS) for i in range(4))
    c = models.Captcha(id=captcha_id, prompt=prompt)
    try:
        db.add(c)
        db.commit()
    except:
        db.rollback()
        raise HTTPException(
            status_code=status.HTTP_418_IM_A_TEAPOT,
        )
    captcha = ImageCaptcha()
    data = captcha.generate(prompt)
    return data


def check_captcha(
    id: uuid.UUID, prompt: str, db: Annotated[Session, Depends(get_db)]
) -> bool:
    c = (
        db.query(models.Captcha)
        .filter(
            models.Captcha.id == id,
            models.Captcha.prompt == prompt,
            models.Captcha.used == False,
        )
        .first()
    )
    if c:
        setattr(c, "used", True)
        db.commit()
        return True
    return False

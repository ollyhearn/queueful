from typing import Union
from pydantic import BaseModel
from uuid import UUID


class User(BaseModel):
    username: str
    name: Union[str, None] = None


class UserInDB(User):
    id: UUID

    class Config:
        from_attributes = True


class Captcha(BaseModel):
    id: UUID

    class Config:
        from_attributes = True


class CaptchaCheck(BaseModel):
    id: UUID
    prompt: str


class UserRegister(User):
    password: str
    password2: str
    captcha: CaptchaCheck


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Union[str, None] = None


class AnonUser(BaseModel):
    id: UUID
    name: Union[str, None] = None

    class Config:
        from_attributes = True


class AnonUserPatch(BaseModel):
    name: str

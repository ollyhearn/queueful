from typing import Union, List
from pydantic import BaseModel
from uuid import UUID
from ..auth import schemas as auth_schemas


class QueueGroup(BaseModel):
    name: str
    priority: int


class QueueGroupDetail(QueueGroup):
    id: UUID

    class Config:
        from_attributes = True


class QueueUser(BaseModel):
    id: UUID
    position: int
    passed: bool
    group_id: UUID | None = None
    user: auth_schemas.AnonUser

    class Config:
        from_attributes = True


class ParticipantInfo(BaseModel):
    total: int
    remaining: int
    users_list: List[QueueUser]


class Queue(BaseModel):
    name: str
    description: Union[str, None] = None
    groups: List[QueueGroup] | None = None


class QueueInList(Queue):
    participants: ParticipantInfo

    class Config:
        from_attributes = True


class QueueInDb(BaseModel):
    id: UUID
    name: str
    description: Union[str, None] = None

    class Config:
        from_attributes = True


class QueueDetail(Queue):
    id: UUID
    status: str
    owner_id: UUID
    participants: ParticipantInfo
    groups: List[QueueGroupDetail] | None


class ActionResult(BaseModel):
    action: str
    result: str

    class Config:
        from_attributes = True


class JoinRequest(BaseModel):
    group_id: UUID | None = None

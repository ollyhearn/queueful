from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime, types
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
import uuid
import datetime

from .database import Base


class ChoiceType(types.TypeDecorator):

    impl = types.String

    def __init__(self, choices, **kw):
        self.choices = dict(choices)
        super(ChoiceType, self).__init__(**kw)

    def process_bind_param(self, value, dialect):
        return [k for k, v in self.choices.items() if v == value][0]

    def process_result_value(self, value, dialect):
        return self.choices[value]


class User(Base):
    __tablename__ = "users"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String, index=True)
    username = Column(String(length=24), unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)

    owns_queues = relationship("Queue", backref="owner", lazy="dynamic")


class News(Base):
    __tablename__ = "news"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    title = Column(String)
    content = Column(String)
    created = Column(DateTime, default=datetime.datetime.utcnow)
    taps = Column(Integer, default=0)


class AnonymousUser(Base):
    __tablename__ = "anonymoususers"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String, index=True)

    parts_in_queues = relationship("QueueUser", backref="user", lazy="dynamic")


class Queue(Base):
    __tablename__ = "queues"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    created = Column(DateTime, default=datetime.datetime.utcnow)
    name = Column(String, index=True)
    description = Column(String, index=True)
    owner_id = Column(UUID(as_uuid=True), ForeignKey("users.id"))
    start_time = Column(DateTime, nullable=True)
    status = Column(
        ChoiceType(
            {
                "created": "created",
                "waiting": "waiting",
                "active": "active",
                "finished": "finished",
            }
        ),
        nullable=False,
    )

    users = relationship("QueueUser", backref="queue", lazy="dynamic")
    logs = relationship("QueueLog", backref="queue", lazy="dynamic")
    groups = relationship("QueueGroup", backref="queue", lazy="dynamic")


class QueueUser(Base):
    __tablename__ = "queueuser"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    user_id = Column(UUID(as_uuid=True), ForeignKey("anonymoususers.id"))
    queue_id = Column(UUID(as_uuid=True), ForeignKey("queues.id"))
    position = Column(Integer)
    passed = Column(Boolean, default=False)
    group_id = Column(UUID(as_uuid=True), ForeignKey("queuegroup.id"), nullable=True)


class QueueLog(Base):
    __tablename__ = "queuelog"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    action = Column(String)
    queue_id = Column(UUID(as_uuid=True), ForeignKey("queues.id"))
    created = Column(DateTime, default=datetime.datetime.utcnow)


class QueueGroup(Base):
    __tablename__ = "queuegroup"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String)
    priority = Column(Integer)
    queue_id = Column(UUID(as_uuid=True), ForeignKey("queues.id"))

    users = relationship("QueueUser", backref="group", lazy="dynamic")


class Captcha(Base):
    __tablename__ = "captcha"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    prompt = Column(String(length=6))
    used = Column(Boolean, default=False)

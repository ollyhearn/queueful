import React, { useContext } from "react";
import { QueueUser } from "../../slice/AuthApi";
import "../styles.css";
import tr from "../../config/translation";
import { useSelector } from "react-redux";
import { StorePrototype } from "../../config/store";
import { Button } from "antd";
import { QueueDetail, usePassQueueActionMutation } from "../../slice/QueueApi";
import { MessageContext } from "../../App";

const UUIDToColor = (uuid: string): string => {
  return (
    "#" +
    uuid.split("-").reduce((store: string, v: string) => {
      return store + v.substring(0, 1);
    }, "") +
    uuid.substring(uuid.length - 1)
  );
};

const getProfileText = (name: string): string => {
  return name.substring(0, 1);
};

const AnonUserCard = (props: {
  queueUser: QueueUser;
  queue: QueueDetail | undefined;
  refetch: () => void;
}): JSX.Element => {
  const messageApi = useContext(MessageContext);

  const clientId = useSelector((state: StorePrototype) => state.auth.clientId);
  const [passAction] = usePassQueueActionMutation();

  const passQueue = () => {
    if (props.queue) {
      passAction(props.queue.id)
        .unwrap()
        .then(props.refetch)
        .then(() => messageApi.success(tr("You left the queue")))
        .catch(() => messageApi.error(tr("Failed to left")));
    }
  };

  return (
    <div className="anon-card">
      <div
        style={{
          display: "flex",
          gap: "0.5rem",
          alignItems: "center",
          flexFlow: "row wrap",
        }}
      >
        <span style={{ marginRight: "1rem" }}>#{props.queueUser.position}</span>
        <div
          className="anon-circle"
          style={{ background: UUIDToColor(props.queueUser.user.id) }}
        >
          {props.queueUser.user.name
            ? getProfileText(props.queueUser.user.name)
            : props.queueUser.id.substring(0, 2)}
        </div>
        <p color="white" style={{ marginLeft: "10px" }}>
          {props.queueUser.user.name
            ? props.queueUser.user.name
            : tr("Anonymous") + " #" + props.queueUser.id.substring(0, 4)}
        </p>
        {props.queueUser.group_id && (
          <p color="white" style={{ marginLeft: "10px" }}>
            {tr("Group") +
              ": " +
              props.queue?.groups.find(
                (ele) => ele.id === props.queueUser.group_id
              )?.name}
          </p>
        )}
        {props.queueUser && clientId === props.queueUser.user.id && (
          <span
            style={{
              background: "#00d8a4",
              marginLeft: "1rem",
              marginRight: "1rem",
              marginBottom: "0",
              borderRadius: "5px",
              padding: "2px",
            }}
          >
            {tr("YOU")}
          </span>
        )}
        {props.queueUser &&
          clientId === props.queueUser.user.id &&
          props.queueUser.position === 0 &&
          props.queue?.status === "active" && (
            <span
              style={{
                marginLeft: "1rem",
                marginRight: "1rem",
                marginBottom: "0",
                borderRadius: "5px",
                padding: "2px",
              }}
            >
              {tr("It is your turn!")}
            </span>
          )}
      </div>
      <div
        style={{
          display: "flex",
          gap: "0.5rem",
          alignItems: "center",
          flexFlow: "row wrap",
        }}
      >
        {props.queueUser &&
          clientId === props.queueUser.user.id &&
          (props.queueUser.position === 0 &&
          props.queue?.status === "active" ? (
            <Button onClick={() => passQueue()}>{tr("Pass")}</Button>
          ) : (
            <Button onClick={() => passQueue()}>{tr("Leave")}</Button>
          ))}
      </div>
    </div>
  );
};

export default AnonUserCard;

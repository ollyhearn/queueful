import React, { useState } from "react";
import "../styles.css";
import { Button, Divider, Input } from "antd";
import { CameraOutlined } from "@ant-design/icons";
import Title from "antd/es/typography/Title";
import tr from "../../config/translation";
import { useNavigate } from "react-router-dom";

const getLink = (v: string) => {
  if (v.includes("v")) {
    return "/queue/join/" + v.split("/").slice(-1).pop();
  }
  return `/queue/join/${v}`;
};

const JoinQueueCard = (): JSX.Element => {
  const navigate = useNavigate();
  const [value, setValue] = useState("");
  const processLink = () => {
    navigate(getLink(value));
  };
  return (
    <div className="card">
      <CameraOutlined />
      <Title level={3}>{tr("QR-code scanner in development")}</Title>
      <Divider>{tr("OR")}</Divider>
      <Title level={3}>{tr("Join queue by link or id")}</Title>
      <div
        style={{
          width: "100%",
          display: "flex",
          flexFlow: "row",
        }}
      >
        <Input
          style={{ width: "100rem" }}
          placeholder={tr("Queue link or id")}
          onChange={(e) => setValue(e.target.value)}
        />
        <Button type="primary" onClick={() => processLink()}>
          {tr("Join")}
        </Button>
      </div>
    </div>
  );
};
export default JoinQueueCard;

import React, { useEffect } from "react";
import { useGetQueuesQuery } from "../../slice/QueueApi";
import "../styles.css";
import { Button, Spin } from "antd";
import { LoadingOutlined, RightOutlined } from "@ant-design/icons";
import Title from "antd/es/typography/Title";
import tr from "../../config/translation";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { StorePrototype } from "../../config/store";

type Queue = {
  id: string;
  name: string;
};

const PartingQueuesList = (): JSX.Element => {
  const user = useSelector((state: StorePrototype) => state.auth.user);
  const { data, refetch, isLoading } = useGetQueuesQuery({});
  useEffect(() => {
    user && refetch();
  }, [user]);
  return (
    <div className="card">
      <Title level={2}>{tr("Queues you are in")}</Title>
      <br />
      <br />
      <Spin
        indicator={<LoadingOutlined style={{ fontSize: 36 }} spin />}
        spinning={isLoading}
      >
        {data?.length ? (
          data?.map((ele: Queue) => (
            <div className="card secondary queue-in-list" key={ele.id}>
              <Title level={4}>{ele.name}</Title>
              <Link to={`/queue/${ele.id}`}>
                <Button
                  icon={<RightOutlined />}
                  type="primary"
                  size="large"
                  style={{ height: "100%", width: "4rem" }}
                />
              </Link>
            </div>
          ))
        ) : (
          <>
            <Title level={3}>{tr("You are not parting in any queues!")}</Title>
            <div className="button-box">
              <Link to="/queue/join">
                <Button size="large">{tr("Join a queue")}</Button>
              </Link>
            </div>
          </>
        )}
      </Spin>
    </div>
  );
};
export default PartingQueuesList;

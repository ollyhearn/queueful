import React, { useContext, useState } from "react";
import {
  useGetQueueDetailQuery,
  useKickFirstActionMutation,
  useStartQueueActionMutation,
} from "../../slice/QueueApi";
import "../styles.css";
import { Button, QRCode, Spin } from "antd";
import {
  FieldTimeOutlined,
  FileTextOutlined,
  FlagOutlined,
  HourglassOutlined,
  LoadingOutlined,
  PlusCircleOutlined,
  QuestionCircleOutlined,
  UserOutlined,
  ZoomInOutlined,
  ZoomOutOutlined,
} from "@ant-design/icons";
import Title from "antd/es/typography/Title";
import tr from "../../config/translation";
import { useParams } from "react-router-dom";
import AnonUserCard from "../user/AnonUserCard";
import { useSelector } from "react-redux";
import { StorePrototype } from "../../config/store";
import { MessageContext } from "../../App";
import { baseClientUrl } from "../../config/baseUrl";

const getStatusText = (status: string) => {
  switch (status) {
    case "created":
      return (
        <p>
          <PlusCircleOutlined /> {"  "}
          {tr("Created")}
        </p>
      );
    case "waiting":
      return (
        <p>
          <HourglassOutlined /> {"  "}
          {tr("Waiting for start")}
        </p>
      );
    case "active":
      return (
        <p>
          <FieldTimeOutlined /> {"  "}
          {tr("In progress")}
        </p>
      );
    case "finished":
      return (
        <p>
          <FlagOutlined /> {"  "}
          {tr("Finished")}
        </p>
      );
    default:
      return (
        <p>
          <QuestionCircleOutlined /> {"  "}
          {tr("Unknown status")}
        </p>
      );
  }
};

const QueueCard = (): JSX.Element => {
  const messageApi = useContext(MessageContext);

  const { queueId } = useParams();
  const { data, isFetching, refetch, error } = useGetQueueDetailQuery(queueId, {
    skip: !queueId,
  });
  const user = useSelector((state: StorePrototype) => state.auth.user);
  const [kickFirstAction] = useKickFirstActionMutation();
  const [startQueueAction] = useStartQueueActionMutation();

  const [qrShown, setQrShown] = useState(false);
  const [largeQr, setLargeQr] = useState(false);

  const kickFirst = () => {
    if (queueId) {
      kickFirstAction(queueId)
        .unwrap()
        .then(() => refetch())
        .then(() => messageApi.success(tr("First user in queue kicked!")))
        .catch(() => messageApi.error(tr("Action error")));
    }
  };

  const startQueue = () => {
    if (queueId) {
      startQueueAction(queueId)
        .unwrap()
        .then(() => refetch())
        .then(() => messageApi.success(tr("Queue has started!")))
        .catch(() => messageApi.error(tr("Action error")));
    }
  };

  const getJoinLink = () => {
    if (data) {
      return baseClientUrl + `/queue/join/${data.id}`;
    }
    return baseClientUrl;
  };

  const copyJoinLink = async () => {
    if (data) {
      try {
        await navigator.clipboard.writeText(getJoinLink());
        messageApi.success(tr("Copied!"));
      } catch (error) {
        messageApi.error(tr("Error occured!"));
      }
    }
  };

  if (!error) {
    return (
      <div className="card">
        <Spin
          indicator={<LoadingOutlined style={{ fontSize: 36 }} spin />}
          spinning={isFetching}
        >
          <div className="queue-info">
            <Title level={3} style={{ textAlign: "left" }}>
              {data?.name}
            </Title>
            <p>
              <FileTextOutlined />
              {"  "}
              {data?.description}
            </p>
            <p>
              <UserOutlined />
              {"  "}
              {data?.participants?.remaining} / {data?.participants?.total}
            </p>
            {data && getStatusText(data.status)}
            {data && user && user.id === data.owner_id && (
              <div style={{ display: "flex", flexFlow: "row wrap" }}>
                <Button onClick={kickFirst}>{tr("Kick first")}</Button>
                {data?.status === "created" && (
                  <Button onClick={startQueue}>{tr("Start queue")}</Button>
                )}
                <Button onClick={copyJoinLink}>{tr("Copy join link")}</Button>
                {qrShown ? (
                  <Button onClick={() => setQrShown(false)}>
                    {tr("Hide QR-code")}
                  </Button>
                ) : (
                  <Button onClick={() => setQrShown(true)}>
                    {tr("Show QR-code")}
                  </Button>
                )}
              </div>
            )}
          </div>
          {data && qrShown && (
            <div style={{ display: "flex", flexFlow: "row nowrap" }}>
              <QRCode
                errorLevel="H"
                value={getJoinLink()}
                icon={
                  baseClientUrl + "/static/image/android-chrome-512x512.png"
                }
                size={largeQr ? 320 : 160}
              />
              <Button
                icon={largeQr ? <ZoomOutOutlined /> : <ZoomInOutlined />}
                onClick={() => setLargeQr((v) => !v)}
              />
            </div>
          )}

          <div>
            <Title level={3} style={{ textAlign: "left" }}>
              {tr("Queue participants")}
            </Title>
            {data?.participants.users_list.map((v) => {
              return (
                <AnonUserCard
                  key={v.id}
                  queueUser={v}
                  queue={data}
                  refetch={refetch}
                />
              );
            })}
          </div>
        </Spin>
      </div>
    );
  }

  return (
    <>
      <div style={{ width: "100%", marginTop: "3rem" }}>
        <QuestionCircleOutlined style={{ fontSize: "5rem" }} />
      </div>
      <Title>{tr("Queue not found")}</Title>
      <Title level={3}>404</Title>
    </>
  );
};
export default QueueCard;

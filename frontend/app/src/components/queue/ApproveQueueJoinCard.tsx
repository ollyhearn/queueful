import React, { useContext, useState } from "react";
import "../styles.css";
import { Button, Input, Select, Spin } from "antd";
import {
  ArrowLeftOutlined,
  FileTextOutlined,
  PlusOutlined,
  UserOutlined,
} from "@ant-design/icons";
import Title from "antd/es/typography/Title";
import tr from "../../config/translation";
import { useNavigate } from "react-router-dom";
import {
  useGetQueueDetailQuery,
  useJoinQueueMutation,
} from "../../slice/QueueApi";
import { MessageContext } from "../../App";
import { usePatchAnonMutation } from "../../slice/AuthApi";

const ApproveQueueJoinCard = (props: { id: string }): JSX.Element => {
  const navigate = useNavigate();
  const messageApi = useContext(MessageContext);

  const { data, refetch, isFetching, isError } = useGetQueueDetailQuery(
    props.id,
    {
      skip: !props.id,
    }
  );
  const [joinQueue, { isLoading }] = useJoinQueueMutation();
  const [patchAnon] = usePatchAnonMutation();
  const [newName, setNewName] = useState("");
  const [selectedGroup, setSelectedGroup] = useState<string>();

  const onJoinButtonClick = () => {
    joinQueue({ queueId: props.id, data: { group_id: selectedGroup } })
      .unwrap()
      .then(() => navigate(`/queue/${props.id}`))
      .then(() => refetch())
      .then(() => messageApi.success(tr("Successfully joined queue")))
      .catch((e) => messageApi.error(tr(e.data.detail)));
  };

  const patchName = () => {
    patchAnon({ name: newName })
      .unwrap()
      .then(() => messageApi.success(tr("Successfully changed name")))
      .catch(() => messageApi.error(tr("Error changing name")));
  };

  return (
    <div className="card">
      <Spin spinning={isFetching}>
        {isError ? (
          <>
            <Title level={3}>{tr("Queue not found!")}</Title>
            <Button
              icon={<ArrowLeftOutlined />}
              type="primary"
              onClick={() => navigate("/queue/join")}
            >
              {tr("Go back")}
            </Button>
          </>
        ) : (
          <div className="queue-info">
            <Spin spinning={isLoading}>
              <Title level={3} style={{ textAlign: "left" }}>
                {data?.name}
              </Title>
              <p>
                <FileTextOutlined />
                {"  "}
                {data?.description}
              </p>
              <p>
                <UserOutlined />
                {"  "}
                {data?.participants?.remaining} / {data?.participants?.total}
              </p>
              <Title level={4}>{tr("Update your name")}</Title>
              <div style={{ display: "flex", flexFlow: "row", width: "30vw" }}>
                <Input
                  value={newName}
                  onChange={(e) => setNewName(e.target.value)}
                  placeholder={tr("Enter new name")}
                />
                <Button style={{ marginLeft: "1rem" }} onClick={patchName}>
                  {tr("Update")}
                </Button>
              </div>
              {data?.groups.length !== undefined && data?.groups.length > 0 && (
                <>
                  <Title level={4}>{tr("Select group in queue")}</Title>
                  <Select
                    options={data?.groups.map((obj) => ({
                      value: obj.id,
                      label: obj.name,
                    }))}
                    placeholder={tr("Select group...")}
                    value={selectedGroup}
                    style={{ width: "30vw", marginTop: "1rem" }}
                    onChange={setSelectedGroup}
                  />
                </>
              )}

              <Button
                style={{ width: "100%", marginTop: "2rem" }}
                type="primary"
                icon={<PlusOutlined />}
                onClick={onJoinButtonClick}
              >
                {tr("Join")}
              </Button>
            </Spin>
          </div>
        )}
      </Spin>
    </div>
  );
};
export default ApproveQueueJoinCard;

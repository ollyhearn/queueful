import React, { useContext, useReducer, useState } from "react";
import {
  CreateQueue,
  CreateQueueRequest,
  Queue,
  useCreateQueueMutation,
} from "../../slice/QueueApi";
import "../styles.css";
import { Button, Form, Input, List, Spin, Tooltip, Typography } from "antd";
import Title from "antd/es/typography/Title";
import tr from "../../config/translation";
import { MessageContext } from "../../App";
import { useNavigate } from "react-router-dom";
import {
  DeleteOutlined,
  DownOutlined,
  PlusCircleOutlined,
  QuestionCircleOutlined,
  UpOutlined,
} from "@ant-design/icons";

const CreateQueueCard = (): JSX.Element => {
  const messageApi = useContext(MessageContext);
  const navigate = useNavigate();

  const [form] = Form.useForm();
  const [createQueue, { isLoading }] = useCreateQueueMutation();

  const [newGroupValue, setNewGroupValue] = useState("");
  const [groupsList, setGroupsList] = useState<[{ name: string; id: string }]>([
    { name: tr("Default"), id: "default" },
  ]);
  const [, forceUpdate] = useReducer((x) => x + 1, 0);

  const pushGroupUp = (element: { name: string; id: string }) => {
    const index = groupsList.indexOf(element);
    if (index > 0) {
      const newArr = groupsList;
      const temp = newArr[index - 1];
      newArr[index - 1] = element;
      newArr[index] = temp;
      setGroupsList(newArr);
      forceUpdate();
    }
  };

  const pushGroupDown = (element: { name: string; id: string }) => {
    const index = groupsList.indexOf(element);
    if (index < groupsList.length - 1) {
      const newArr = groupsList;
      const temp = newArr[index + 1];
      newArr[index + 1] = element;
      newArr[index] = temp;
      setGroupsList(newArr);
      forceUpdate();
    }
  };

  const deleteGroup = (element: { name: string; id: string }) => {
    if (groupsList.includes(element)) {
      const newArr = groupsList;
      newArr.splice(groupsList.indexOf(element), 1);
      setGroupsList(newArr);
      forceUpdate();
    }
  };

  type OrderedGroup = { name: string; priority: number };

  const getOrderedGroups = () => {
    const divId = groupsList.findIndex((ele) => ele.id === "default");
    if (divId || divId === 0) {
      const arr1: OrderedGroup[] = [];
      groupsList
        .slice(0, divId)
        .reverse()
        .forEach((ele, i) => arr1.push({ name: ele.name, priority: -(i + 1) }));
      const arr2: OrderedGroup[] = [];
      groupsList
        .slice(divId + 1)
        .forEach((ele, i) => arr2.push({ name: ele.name, priority: i + 1 }));
      return [...arr1, ...arr2];
    }
    return [];
  };

  getOrderedGroups();

  const submit = (formData: CreateQueue) => {
    createQueue({ ...formData, groups: getOrderedGroups() })
      .unwrap()
      .then((data: Queue) => navigate(`/queue/${data.id}`))
      .then(() => messageApi.success(tr("Queue created")))
      .catch(() => messageApi.error(tr("Failed to create queue")));
  };

  const addGroupToList = () => {
    if (newGroupValue) {
      const tmp = groupsList;
      if (tmp) {
        tmp.push({
          name: newGroupValue,
          id: tmp.length.toString(),
        });
        setGroupsList(tmp);
      } else {
        setGroupsList((v) => [
          {
            name: newGroupValue,
            id: v.length.toString(),
          },
        ]);
      }
      setNewGroupValue("");
    }
  };

  return (
    <div className="card">
      <Spin spinning={isLoading}>
        <Title level={2}>{tr("New queue")}</Title>
        <Form
          form={form}
          layout="vertical"
          requiredMark={false}
          onFinish={(formData: CreateQueueRequest) => submit(formData)}
        >
          <Form.Item
            name={"name"}
            label={tr("Name")}
            rules={[
              {
                required: true,
                message: tr("Please input queue name!"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"description"}
            label={tr("Description") + " " + tr("(optional)")}
          >
            <Input />
          </Form.Item>
          <Form.Item>
            <List
              dataSource={groupsList}
              header={
                <div style={{ display: "flex", gap: "1rem" }}>
                  <Title level={4}>
                    {tr("Queue groups") + " " + tr("(optional)")}
                  </Title>
                  <Tooltip
                    title={tr(
                      "Queue's groups function allows you to define groups inside this particular queue. Groups have name and priority. Every participant selects theirs group for this queue, and their position is calculated by group's priority. Groups higher in the list will have higher priority."
                    )}
                  >
                    <QuestionCircleOutlined />
                  </Tooltip>
                </div>
              }
              footer={
                <div style={{ display: "flex", gap: "1rem" }}>
                  <Input
                    placeholder={tr("Group's name")}
                    value={newGroupValue}
                    onChange={(e) => setNewGroupValue(e.target.value)}
                    onPressEnter={addGroupToList}
                  />
                  <Button onClick={addGroupToList}>{tr("Add group")}</Button>
                </div>
              }
              renderItem={(item, index) => (
                <List.Item
                  style={{
                    display: "flex",
                    alignContent: "space-between",
                    gap: "1rem",
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      alignContent: "space-between",
                      gap: "1rem",
                    }}
                  >
                    <Typography.Text>#{index + 1}</Typography.Text>
                    <Typography.Text>{item.name}</Typography.Text>
                  </div>
                  {item.id !== "default" && (
                    <div style={{ display: "flex" }}>
                      <Button
                        onClick={() => pushGroupUp(item)}
                        icon={<UpOutlined />}
                        disabled={groupsList.indexOf(item) === 0}
                      />

                      <Button
                        icon={<DownOutlined />}
                        onClick={() => pushGroupDown(item)}
                        disabled={
                          groupsList.indexOf(item) === groupsList.length - 1
                        }
                      />
                      <Button
                        icon={<DeleteOutlined />}
                        onClick={() => deleteGroup(item)}
                      />
                    </div>
                  )}
                </List.Item>
              )}
            ></List>
          </Form.Item>
          <Button
            style={{ width: "100%" }}
            icon={<PlusCircleOutlined />}
            type="primary"
            onClick={() => form.submit()}
          >
            {tr("Create")}
          </Button>
        </Form>
      </Spin>
    </div>
  );
};
export default CreateQueueCard;

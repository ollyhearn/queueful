import {
  Button,
  Carousel,
  Form,
  Input,
  Menu,
  MenuProps,
  Modal,
  Spin,
} from "antd";
import {
  KeyOutlined,
  ReloadOutlined,
  UserAddOutlined,
} from "@ant-design/icons";
import React, { useContext, useEffect, useState } from "react";
import {
  TokenResponse,
  useGetClientQuery,
  useGetUserQuery,
  useLoginMutation,
  useRegisterMutation,
} from "../slice/AuthApi";
import { MessageContext } from "../App";
import { store, updateClient, updateToken, updateUser } from "../config/store";
import tr from "../config/translation";
import { useNavigate } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import { baseUrl } from "../config/baseUrl";
import { CarouselRef } from "antd/es/carousel";

const AuthModal = (props: {
  open: boolean;
  setOpen: (arg0: boolean) => void;
  setDrawerOpen: (arg0: boolean) => void;
}) => {
  const navigate = useNavigate();
  const messageApi = useContext(MessageContext);
  const carousel = React.createRef<CarouselRef>();

  const [loginForm] = Form.useForm();
  const [registerForm] = Form.useForm();

  const [captchaId, setCaptchaId] = useState("");
  const [captchaPic, setCaptchaPic] = useState("");
  const [captchaFetching, setCaptchaFetching] = useState(false);

  const fetchCaptcha = async () => {
    setCaptchaFetching(true);
    registerForm.setFieldValue(["captcha", "prompt"], "");
    const id = uuidv4();
    setCaptchaId(id);
    const res = await fetch(`${baseUrl}/auth/captcha/${id}`);
    const imageBlob = await res.blob();
    const imageObjectURL = URL.createObjectURL(imageBlob);
    setCaptchaPic(imageObjectURL);
    setCaptchaFetching(false);
  };

  const { data, refetch, isFetching, isError } = useGetUserQuery({});
  useEffect(() => {
    if (!isFetching && !isError) {
      store.dispatch(updateUser(data));
    }
  }, [data, isFetching, useGetUserQuery]);

  const {
    data: clientData,
    isFetching: isFetchingClient,
    isError: isErrorClient,
  } = useGetClientQuery({});
  useEffect(() => {
    if (!isFetchingClient) {
      if (isErrorClient) {
        store.dispatch(updateClient(null));
      } else {
        store.dispatch(updateClient(clientData.id));
      }
    }
  }, [clientData, isFetchingClient, useGetClientQuery]);

  const [current, setCurrent] = useState("login");
  useEffect(() => {
    if (carousel && carousel.current && carousel?.current !== undefined) {
      carousel.current.goTo(["login", "register"].indexOf(current));
    }
  }, [current]);

  const [loginUser, { isLoading: isLoggingIn }] = useLoginMutation();
  const [registerUser, { isLoading: isRegistering }] = useRegisterMutation();

  const submitLoginForm = (data: { username: string; password: string }) => {
    const formData = new FormData();
    formData.append("username", data.username);
    formData.append("password", data.password);

    loginUser(formData)
      .unwrap()
      .then((data: TokenResponse) => {
        store.dispatch(updateToken(data.access_token));
      })
      .then(() => loginForm.resetFields())
      .then(() => refetch())
      .then(() => props.setOpen(false))
      .then(() => navigate("/dashboard"))
      .then(() => props.setDrawerOpen(false))
      .catch((e) => messageApi.error(tr(e.data.detail)));
  };

  const submitRegisterForm = (formData: {
    username: string;
    name: string | undefined;
    password: string;
    password2: string;
    captcha: { id: string; prompt: string };
  }) => {
    formData.captcha.id = captchaId;
    registerUser(formData)
      .unwrap()
      .then(() => submitLoginForm(formData))
      .then(() => props.setOpen(false))
      .then(() => setCaptchaPic(""))
      .then(() => registerForm.resetFields())
      .catch((e) => {
        messageApi.error(tr(e.data.detail));
        fetchCaptcha();
      });
  };

  const items: MenuProps["items"] = [
    {
      label: tr("Log in"),
      key: "login",
      icon: <KeyOutlined />,
    },
    {
      label: tr("Register"),
      key: "register",
      icon: <UserAddOutlined />,
    },
  ];

  return (
    <Modal
      open={props.open}
      onCancel={() => props.setOpen(false)}
      onOk={() => {
        current === "register" && registerForm.submit();
        current === "login" && loginForm.submit();
      }}
      okText={current === "login" ? tr("Log in") : tr("Register")}
      confirmLoading={isLoggingIn}
    >
      <Spin spinning={isLoggingIn || isRegistering}>
        <div
          style={{ display: "flex", width: "100%", justifyContent: "center" }}
        >
          <Menu
            onClick={(e) => setCurrent(e.key)}
            mode="horizontal"
            selectedKeys={[current]}
            items={items}
            style={{ width: "fit-content" }}
            disabledOverflow={true}
          />
        </div>
        <br />
        <Carousel
          ref={carousel}
          dots={false}
          speed={200}
          onSwipe={(dir) => setCurrent(dir === "left" ? "register" : "login")}
          infinite={false}
        >
          <Form
            form={loginForm}
            onFinish={submitLoginForm}
            layout="vertical"
            requiredMark={false}
          >
            <Form.Item
              name="username"
              label={tr("Username")}
              rules={[
                {
                  required: true,
                  message: tr("Please input your Username!"),
                },
              ]}
            >
              <Input autoFocus />
            </Form.Item>
            <Form.Item
              name="password"
              label={tr("Password")}
              rules={[
                {
                  required: true,
                  message: tr("Please input your Password!"),
                },
              ]}
            >
              <Input type="password" onPressEnter={() => loginForm.submit()} />
            </Form.Item>
          </Form>
          <Form
            form={registerForm}
            onFinish={submitRegisterForm}
            layout="vertical"
            requiredMark={false}
          >
            <Form.Item
              name="username"
              label={tr("Username")}
              rules={[
                {
                  required: true,
                  message: tr("Please input your Username!"),
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item name="name" label={tr("Display name")}>
              <Input />
            </Form.Item>
            <Form.Item
              name="password"
              label={tr("Password")}
              rules={[
                {
                  required: true,
                  message: tr("Please input your password!"),
                },
              ]}
            >
              <Input type="password" />
            </Form.Item>
            <Form.Item
              name="password2"
              label={tr("Repeat password")}
              rules={[
                {
                  required: true,
                  message: tr("Please confirm your Password!"),
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      new Error(
                        tr("The new password that you entered do not match!")
                      )
                    );
                  },
                }),
              ]}
            >
              <Input type="password" />
            </Form.Item>
            <Form.Item label={tr("Captcha")}>
              {captchaId ? (
                <Spin spinning={captchaFetching}>
                  <img
                    onClick={() => fetchCaptcha()}
                    src={captchaPic}
                    alt={tr("Captcha")}
                  />
                </Spin>
              ) : (
                <Button
                  onClick={() => fetchCaptcha()}
                  icon={<ReloadOutlined />}
                >
                  {tr("Fetch captcha")}
                </Button>
              )}
            </Form.Item>
            <Form.Item
              name={["captcha", "prompt"]}
              label={tr("Captcha prompt")}
              rules={[
                {
                  required: true,
                  message: tr("Please enter captcha!"),
                },
              ]}
            >
              <Input
                disabled={!captchaId}
                onPressEnter={() => registerForm.submit()}
              />
            </Form.Item>
          </Form>
        </Carousel>
      </Spin>
    </Modal>
  );
};

export default AuthModal;

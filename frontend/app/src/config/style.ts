import { ThemeConfig } from "antd";

export const darkTheme: ThemeConfig = {
  token: {
    colorText: "white",
    colorIcon: "white",
    colorPrimary: "#00d8a4",
    colorIconHover: "#00d8a4",
    colorBgContainer: "#001d39",
    colorBgBase: "#001529",
    borderRadius: 5,
    fontFamily: "Comfortaa",
    colorWarningBg: "#001529",
  },
  components: {
    Input: {
      activeBorderColor: "#001529",
      colorTextPlaceholder: "grey",
    },
    Select: {
      colorTextPlaceholder: "grey",
    },
  },
};

export const lightTheme: ThemeConfig = {
  token: {
    colorPrimary: "#00d8a4",
    colorIconHover: "#00d8a4",
    borderRadius: 5,
    fontFamily: "Comfortaa",
    // colorWarningBg: "#001529",
  },
};

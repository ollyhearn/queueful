import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "../config/baseUrl";
import { RootState } from "../config/store";
import { QueueUser } from "./AuthApi";

type OrderedGroup = {
  name: string;
  priority: number;
};

type ResponseGroup = {
  id: string;
  name: string;
  priority: number;
};

export type CreateQueueRequest = {
  name: string;
  description: string | null;
  groups: OrderedGroup[];
};

export type CreateQueue = {
  name: string;
  description: string | null;
};

export type Queue = {
  id: string;
  name: string;
  description: string | null;
};

export type QueueDetail = {
  id: string;
  name: string;
  description: string | null;
  status: string;
  owner_id: string;
  groups: ResponseGroup[];
  participants: {
    total: number;
    remaining: number;
    users_list: [QueueUser];
  };
};

export type JoinRequest = {
  group_id: string | undefined;
};

export const QueueApi = createApi({
  reducerPath: "QueueApi",
  baseQuery: fetchBaseQuery({
    baseUrl: `${baseUrl}/queue`,
    prepareHeaders: (headers, { getState }) => {
      const token = (getState() as RootState).auth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      const clientID = (getState() as RootState).auth.clientId;
      if (clientID) {
        headers.set("X-Client-Id", clientID);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getQueues: builder.query<[Queue], unknown>({
      query: () => "/",
    }),
    getOwnedQueues: builder.query<[Queue], unknown>({
      query: () => "/owned",
    }),
    getQueueDetail: builder.query<QueueDetail, string | undefined>({
      query: (queueId: string | undefined) => `/${queueId}`,
    }),
    joinQueue: builder.mutation({
      query: (args: { queueId: string; data: JoinRequest }) => ({
        url: `/${args.queueId}/join`,
        method: "POST",
        body: args.data,
      }),
    }),
    createQueue: builder.mutation({
      query: (data: CreateQueueRequest) => ({
        url: "/",
        method: "POST",
        body: data,
      }),
    }),
    passQueueAction: builder.mutation({
      query: (queueId: string) => ({
        url: `/${queueId}/action/pass`,
        method: "POST",
      }),
    }),
    kickFirstAction: builder.mutation({
      query: (queueId: string) => ({
        url: `/${queueId}/action/kick-first`,
        method: "POST",
      }),
    }),
    startQueueAction: builder.mutation({
      query: (queueId: string) => ({
        url: `/${queueId}/action/start`,
        method: "POST",
      }),
    }),
  }),
});

export const {
  useGetQueuesQuery,
  useGetOwnedQueuesQuery,
  useGetQueueDetailQuery,
  useJoinQueueMutation,
  useCreateQueueMutation,
  usePassQueueActionMutation,
  useKickFirstActionMutation,
  useStartQueueActionMutation,
} = QueueApi;

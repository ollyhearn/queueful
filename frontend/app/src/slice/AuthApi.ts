import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "../config/baseUrl";
import { RootState } from "../config/store";

export interface User {
  id: string;
  username: string;
  name: string;
}

export interface UserResponse {
  user: User;
  token: string;
}

export interface RegisterRequest {
  username: string;
  name: string | undefined;
  password: string;
  password2: string;
}

export type TokenResponse = {
  access_token: string;
  token_type: string;
};

export type QueueUser = {
  id: string;
  position: number;
  passed: boolean;
  user: AnonUser;
  group_id: string | undefined;
};

export type AnonUser = {
  id: string;
  name: string;
};

export type AnonUserPatch = {
  name: string;
};

export const AuthApi = createApi({
  reducerPath: "AuthApi",
  baseQuery: fetchBaseQuery({
    baseUrl: `${baseUrl}/auth`,
    prepareHeaders: (headers, { getState }) => {
      // By default, if we have a token in the store, let's use that for authenticated requests
      const token = (getState() as RootState).auth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      const clientID = (getState() as RootState).auth.clientId;
      if (clientID) {
        headers.set("X-Client-Id", clientID);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getUser: builder.query({
      query: () => "/me",
    }),
    getClient: builder.query({
      query: () => "/anon",
    }),
    login: builder.mutation({
      query: (data: FormData) => ({
        url: "/token",
        method: "POST",
        body: data,
        formData: true,
      }),
    }),
    register: builder.mutation({
      query: (data: RegisterRequest) => ({
        url: "/register",
        method: "POST",
        body: data,
      }),
    }),
    patchAnon: builder.mutation({
      query: (data: AnonUserPatch) => ({
        url: "/anon",
        method: "PATCH",
        body: data,
      }),
    }),
  }),
});

export const {
  useGetUserQuery,
  useGetClientQuery,
  useLoginMutation,
  useRegisterMutation,
  usePatchAnonMutation,
} = AuthApi;

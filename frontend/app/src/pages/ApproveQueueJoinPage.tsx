import React from "react";
import "./styles.css";
import { useSelector } from "react-redux";
import { StorePrototype } from "../config/store";
import NotFoundPage from "./NotFoundPage";
import ApproveQueueJoinCard from "../components/queue/ApproveQueueJoinCard";
import { useParams } from "react-router-dom";

const ApproveQueueJoinPage = () => {
  const clientId = useSelector((state: StorePrototype) => state.auth.clientId);
  const { queueId } = useParams();
  return clientId && queueId ? (
    <ApproveQueueJoinCard id={queueId} />
  ) : (
    <NotFoundPage />
  );
};

export default ApproveQueueJoinPage;

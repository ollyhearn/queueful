import React from "react";
import "./styles.css";
import { useSelector } from "react-redux";
import { StorePrototype } from "../config/store";
import tr from "../config/translation";
import Title from "antd/es/typography/Title";
import PartingQueuesList from "../components/queue/PartingQueuesList";

const PartingQueuesPage = () => {
  const clientId = useSelector((state: StorePrototype) => state.auth.clientId);
  return clientId ? (
    <PartingQueuesList />
  ) : (
    <Title level={2}>{tr("Whoops!")}</Title>
  );
};

export default PartingQueuesPage;

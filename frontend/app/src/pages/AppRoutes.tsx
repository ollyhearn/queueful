import React, { ReactNode } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import MainPage from "./MainPage";
import {
  getLocalClient,
  getLocalToken,
  loadLanguage,
  loadTheme,
  store,
} from "../config/store";
import DashboardPage from "./DashboardPage";
import PropTypes from "prop-types";
import NotFoundPage from "./NotFoundPage";
import NewQueuePage from "./NewQueuePage";
import NewsPage from "./NewsPage";
import CreateNewsPage from "./CreateNewsPage";
import QueueCard from "../components/queue/QueueCard";
import JoinQueuePage from "./JoinQueuePage";
import ApproveQueueJoinPage from "./ApproveQueueJoinPage";
import PartingQueuesPage from "./PartingQueuesPage";

const AppRoutes = ({ children }: { children: ReactNode }) => {
  store.dispatch(getLocalToken());
  store.dispatch(getLocalClient());
  store.dispatch(loadLanguage());
  store.dispatch(loadTheme());

  return (
    <BrowserRouter>
      {children}
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/dashboard" element={<DashboardPage />} />
        <Route path="/parting" element={<PartingQueuesPage />} />
        <Route path="/queue/:queueId" element={<QueueCard />} />
        <Route path="/queue/join" element={<JoinQueuePage />} />
        <Route path="/queue/join/:queueId" element={<ApproveQueueJoinPage />} />
        <Route path="/dashboard/new" element={<NewQueuePage />} />
        <Route path="/news" element={<NewsPage />} />
        <Route path="/news/new" element={<CreateNewsPage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </BrowserRouter>
  );
};
AppRoutes.propTypes = {
  children: PropTypes.node,
};
export default AppRoutes;
